
# Retina ALPR x86 Windows

## Requirements
Docker for Windows runs on 64-bit Windows 10 Pro, Enterprise, and Education; 1511 November update, Build 10586 or later. Docker plans to support more versions of Windows 10 in the future.

for more SO system versions, see details in [Compability Matrix](https://success.docker.com/article/compatibility-matrix)


## Install Docker
1) [Download Docker](https://download.docker.com/win/beta/InstallDocker.msi)

2) Double-click InstallDocker.msi to run the installer.

3) Follow the Install Wizard: accept the license, authorize the installer, and proceed with the install.

4) Click Finish to launch Docker.

5) Docker starts automatically.

6) Docker loads a “Welcome” window giving you tips and access to the Docker documentation.

That’s it!

for more details in installation process, check out the [official documentation](https://docs.docker.com/docker-for-windows/install/)


## Verification
The whale in the status bar indicates a running (and accessible via terminal) Docker instance.

Open PowerShell or your favorite Windows terminal (e.g., Command prompt) and enter `docker run hello-world`


## Setup Docker

The Docker Desktop menu allows you to configure your Docker settings such as installation, updates, version channels, Docker Hub login, and more.

This section explains the setup options accessible from the Settings dialog.

1) Open the Docker Desktop menu by clicking the Docker icon in the Notifications area (or System tray):

![system tray](https://docs.docker.com/docker-for-windows/images/whale-icon-systray-hidden.png)

2) Select `Settings` to open the Settings dialog:

![Docker Desktop popup menu](https://docs.docker.com/docker-for-windows/images/docker-menu-settings.png)


### Shared drives
Share your local drives (volumes) with Docker Desktop, so that they are available to your Linux containers.

![Shared drives](https://docs.docker.com/docker-for-windows/images/settings-shared-drives.png)

Permission for shared drives are tied to the credentials you provide here. If you run docker commands under a different username than the one configured here, your containers cannot access the mounted volumes.

To apply shared drives, you are prompted for your Windows system (domain) username and password. You can select an option to have Docker store the credentials so that you don’t need to enter them every time.

for more details go to https://docs.docker.com/docker-for-windows/


## Running Retina Vision ALPR

## Open Windows PowerShell
```
# clone retina_alpr_x86 repository
$ git clone https://bitbucket.org/prowl_team/retina_alpr_x86.git

$ cd ./retina_alpr_x86

# change to branch in beta
$ git chckout ph-feature-windows_support

# create directory system
$ mkdir log
$ mkdir log/supervisor
$ mkdir db
$ mkdir images

# run setup
$ cd windows

$ ./setup.bat

$ ./update.bat

$ ./start.bat
```

## Setup
`./setup.bat`

## Update
`./update.bat`

## Start
`./start.bat`

## Stop services
`./stop.bat`

## Adding a RTSP Camera
1) Go to http://localhost:5000/admin/camera, user: admin, pass: admin

2) Go to Configuration > Camera > Create

- Fill the informations

- RTSP (check the camera's manufacturer the RTSP Address)

- Save it

- Check if the status camera at http://localhost:8080/

That's it


## Setup Mirasys and Retina Integrationo

### Mirasys

1) copy `RegexRetinaCfg.xml` to `C:/Program Files/DVMS/DVR`

2) Open Sytem Manager

3) Go to Recorders > Text Channels > Add Channels

- Model: UniversalDataTcpModel
- TCP Port Number: 5050
- Validation: Text
- Configuration File: RegexRetinaCfg.xml
- That's it

### Retina Vision ALPR

#### Setup tcp integration

1) Go to http://localhost:5000/admin, user: admin, pass: admin

2) Go to Configuration > Site > Edit

- TCP Address: (find out machine IPv4 address by typing in powershell `ipconfig`)
- TCP Port: 5050
- That's it, Save it

#### Setup camera feed for capture plate reads

1) Go to http://localhost:5000/admin, user: admin, pass: admin

2) Go to Configuration > Camera > Create:

- Enter Camera Name

- Enter RTSP Camera Address

- Enter Width and Height

- (Optinally) Enter other informations

- That's it, save it


#### Setup Spotter + Retina Vision ALPR viewer

1) Open Spotter

2) Open New Tab
- In Menu Left Bar: double click in Camera, Data Channel
- Add Pligin to tab: Web browser, open `http://localhost:5000/admin/results` user: admin, pass: admin

3) That's it, you've completed the installation and setup process
