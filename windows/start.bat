docker run -d ^
        --name=brain_x86 ^
        --restart unless-stopped ^
        -p=5000:5000 ^
        -p=5555:5555 ^
        -p=8080:8080 ^
        -p=8081:8081 ^
        --volume=%cd%/../db:/etc/owl_client/owl_client/db ^
        --volume=%cd%/../supervisor.conf:/etc/supervisor/conf.d/supervisor.conf ^
        --volume=%cd%/../log:/var/log ^
        retinavision/brain_x86_agent:develop
