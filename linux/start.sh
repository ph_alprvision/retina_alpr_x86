#!/bin/bash
# --volume=$(pwd)/weights:/etc/owl_client/owl_client/files/weights \

docker run -d \
        --name=brain_x86 \
        --restart unless-stopped \
        -p=5000:5000 \
        -p=5555:5555 \
        -p=8080:8080 \
        -p=8081:8081 \
	--volume=$(pwd)/../db:/etc/owl_client/owl_client/db \
        --volume=$(pwd)/../supervisor.conf:/etc/supervisor/conf.d/supervisor.conf \
	--volume=$(pwd)/../images:/etc/owl_client/owl_client/files/images \
	--volume=/var/log:/var/log \
        retinavision/brain_x86_mirasys:develop
